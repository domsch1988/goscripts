package main

import (
	"fmt"
	"log"

	"github.com/arduino/go-apt-client"
)

func main() {
	_, err := apt.CheckForUpdates()
	if err != nil {
		log.Fatal(err)
	}

	updates, err := apt.ListUpgradable()
	if err != nil {
		log.Fatal(err)
	}
	for _, v := range updates {
		fmt.Println(v.Name)
	}
}
