module domsch/apt_go

go 1.20

require github.com/arduino/go-apt-client v0.0.0-20190812130613-5613f843fdc8

require github.com/stretchr/testify v1.8.4 // indirect
