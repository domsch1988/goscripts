package main

import (
	"log"
	"os"
	"os/exec"
	"strings"
)

func main() {
	teamsurl := "teams.microsoft.com"
	url := "--app=" + os.Args[len(os.Args)-1]
	nonteamsurl := os.Args[len(os.Args)-1]
	parsedurl := strings.Split(nonteamsurl, "/")
	teamscmd := exec.Command("flatpak", "run", "--command=/app/bin/chrome", "com.google.Chrome", "--profile-directory=Default", "--app-id=cifhbcnohmdccbgoicgdjpfamggdegmo", url)
	//teamscmd := exec.Command("flatpak", "run", "com.github.IsmaelMartinez.teams_for_linux" , url)
	nonteamscmd := exec.Command("flatpak", "run", "org.mozilla.firefox", nonteamsurl)
	if parsedurl[2] == teamsurl {
		err := teamscmd.Run()
		if err != nil {
			log.Fatal(err)
		}
		teamscmd.Start()
	} else {
		err := nonteamscmd.Run()
		if err != nil {
			log.Fatal(err)
		}
	}
}
