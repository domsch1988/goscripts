package main

import (
	"fmt"
	"math"

	"github.com/mafik/pulseaudio"
)

func main() {

	pulseClient, pulseError := pulseaudio.NewClient()
	if pulseError != nil {
		panic(pulseError)
	}

	pulseOutputs, activeIndex, outErr := pulseClient.Outputs()
	if outErr != nil {
		panic(outErr)
	}

	pulseVolume, volErr := pulseClient.Volume()
	if volErr != nil {
		panic(volErr)
	}

	fmt.Print(math.Round(float64(pulseVolume))*100, "% (", pulseOutputs[activeIndex].CardName, ") ", "\n")

	defer pulseClient.Close()
}
