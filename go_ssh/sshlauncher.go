package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
)

type TreeNode struct {
	Value    string
	Children []*TreeNode
}

func findInventoryFiles(folderPath string, depth int, maxDepth int) (map[string]map[string][]string, error) {
	ipAddressesByFolder := make(map[string]map[string][]string)

	err := filepath.Walk(folderPath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if !info.IsDir() && info.Name() == "inventory" {
			parentFolder := filepath.Dir(path)
			folderName := filepath.Base(parentFolder) // Extracting the folder name
			ipAddresses, err := findIPAddressesInFile(path)
			if err != nil {
				return err
			}

			// Parse INI headings and store IP addresses by heading for the parent folder
			iniHeadings, err := parseIniHeadings(path)
			if err != nil {
				return err
			}
			ipAddressesByFolder[folderName] = iniHeadings

			for heading, ips := range ipAddresses {
				ipAddressesByFolder[folderName][heading] = append(ipAddressesByFolder[folderName][heading], ips...)
			}
		}

		return nil
	})

	if depth < maxDepth {
		subfolders, err := filepath.Glob(filepath.Join(folderPath, "*/"))
		if err != nil {
			return ipAddressesByFolder, err
		}

		for _, subfolder := range subfolders {
			subIPAddresses, err := findInventoryFiles(subfolder, depth+1, maxDepth)
			if err != nil {
				return ipAddressesByFolder, err
			}

			for folder, ipAddresses := range subIPAddresses {
				ipAddressesByFolder[folder] = ipAddresses
			}
		}
	}

	return ipAddressesByFolder, err
}

func findIPAddressesInFile(filePath string) (map[string][]string, error) {
	ipAddresses := make(map[string][]string)

	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		return ipAddresses, err
	}

	ipRegex := regexp.MustCompile(`\b(?:\d{1,3}\.){3}\d{1,3}\b`)
	lines := strings.Split(string(content), "\n")

	var currentHeading string

	for _, line := range lines {
		line = strings.TrimSpace(line)
		if strings.HasPrefix(line, "[") && strings.HasSuffix(line, "]") {
			currentHeading = line[1 : len(line)-1]
		} else {
			matches := ipRegex.FindAllString(line, -1)
			ipAddresses[currentHeading] = append(ipAddresses[currentHeading], matches...)
		}
	}

	return ipAddresses, nil
}

func parseIniHeadings(filePath string) (map[string][]string, error) {
	headings := make(map[string][]string)

	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		return headings, err
	}

	lines := strings.Split(string(content), "\n")

	var currentHeading string

	for _, line := range lines {
		line = strings.TrimSpace(line)
		if strings.HasPrefix(line, "[") && strings.HasSuffix(line, "]") {
			currentHeading = line[1 : len(line)-1]
			headings[currentHeading] = nil
		}
	}

	return headings, nil
}

func buildTree(folder string, data map[string]map[string][]string) *TreeNode {
	root := &TreeNode{Value: folder}

	for heading, ipAddresses := range data[folder] {
		headingNode := &TreeNode{Value: heading}

		for _, ipAddress := range ipAddresses {
			ipNode := &TreeNode{Value: ipAddress}
			headingNode.Children = append(headingNode.Children, ipNode)
		}

		root.Children = append(root.Children, headingNode)
	}

	return root
}

func formatList(node *TreeNode, indent int) string {
	result := strings.Repeat("  ", indent) + node.Value + "\n"

	for _, child := range node.Children {
		result += formatList(child, indent+1)
	}

	return result
}

func main() {
	folderPath := "/home/dosa/ansible-config/environments" // Replace this with the actual folder path you want to search.
	maxDepth := 2                                          // Set the maximum depth for the search.

	ipAddressesByFolder, err := findInventoryFiles(folderPath, 0, maxDepth)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	if len(ipAddressesByFolder) == 0 {
		fmt.Println("No inventory files found.")
		return
	}

	rootFolder := filepath.Base(folderPath)
	rootNode := buildTree(rootFolder, ipAddressesByFolder)
	treeList := formatList(rootNode, 0)

	if err := ui.Init(); err != nil {
		fmt.Printf("failed to initialize termui: %v", err)
		return
	}
	defer ui.Close()

	list := widgets.NewParagraph()
	list.Text = treeList
	list.Title = "Inventory Files and IP Addresses"
	list.TextStyle = ui.NewStyle(ui.ColorYellow)
	list.WrapText = false

	ui.Render(list)

	for e := range ui.PollEvents() {
		if e.Type == ui.KeyboardEvent && e.ID == "q" {
			break
		}
	}
}
