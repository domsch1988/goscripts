package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	db, err := sql.Open("sqlite3", "/home/dosa/.cache/evolution/mail/c6fc9a51aed2ef469874ab73815e528f8e385632/folders.db")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	sqlStmt := `
	select count(*) read from Posteingang where read == 0
	`

	res, err := db.Query(sqlStmt)
	if err != nil {
		log.Fatal(err)
	}
	defer res.Close()
	for res.Next() {
		var (
			read int
		)
		if err := res.Scan(&read); err != nil {
			panic(err)
		}
		fmt.Println(read)
	}
	if err := res.Err(); err != nil {
		panic(err)
	}
}
