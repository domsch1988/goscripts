package main

import (
    "fmt"
    "github.com/shirou/gopsutil/disk"
)

func main() {
    diskStats, diskError := disk.Usage("/")
    diskUsed := (float64(diskStats.Used)/1024/1024/1024)
    diskTotal := diskStats.Total/1024/1024/1024
    diskPercent := diskStats.UsedPercent
    if diskError == nil {
        fmt.Print(fmt.Sprintf("%.2f",diskUsed), "/", diskTotal, "GB (", fmt.Sprintf("%.2f", diskPercent),"%)","\n")
    } else {
        fmt.Println(diskError)
    }
}